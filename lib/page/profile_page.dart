import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          children: [
          customForm("Nama", "Nama", "Harus di isi nama", Icons.person, false),
      customForm("Nomor Telp", "Nomor Telp", "Harus di isi Nomor Telp",Icons.phone, false),
      customForm(
          "Email", "Email", "Harus di isi email", Icons.email, false),
      customForm(
        "Password", "Password", "Harus di isi password", Icons.lock, true)
        ],
      ),
    );
  }

  Padding customForm(String labelText, String hintText, String helperText,
      IconData icon, bool pass) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        decoration: InputDecoration(
          helperText: helperText,
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1, color: Colors.blue),
            borderRadius: BorderRadius.circular(40),
          ),
          labelText: labelText,
          hintText: hintText,
          prefixIcon: Icon(icon, color: Colors.blue),
          suffix: Icon(Icons.chevron_right),
          filled: true,
          fillColor: Colors.white,
        ),
        obscureText: pass,
      ),
    );
  }
}
